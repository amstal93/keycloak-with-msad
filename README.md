# Keycloak with MS Active Directory

Documentation on how connect keycloak with MD Active Directory (run on docker)

## MS Active Directory setup and configuration

Assuming you already have installed Windows Server 2016+, you have to activate 
Active Directory Domain Services, DNS Server and Active Directory Certificate
Services.

### Add Active Directory Domain Services and DNS Server roles

* [ ] Before starting, rename you server if needed (in my case, `dc01`) - reboot if needed
* [ ] Configure you DNS settings: 127.0.0.1 as preferred DNS Server, 208.67.222.222 (OpenDNS) as alternate DNS Server
* [ ] Go to server manager > Manage > Add Roles and Features
* [ ] Skip Before you begin page (`Next >`)
* [ ] Select Role-based or feature-based installation (`Next >`)
* [ ] Check `Select a server from the server pool` and check you domain controller is selected (`Next >`)
* [ ] Select Active Directory Domain Services and click on `Add Features` in the popup (`Next >`)
* [ ] In Features step, keep default feature selection (`Next >`)
* [ ] In AD DS step, read installation details (`Next >`)
* [ ] In Confirmation step, keep Restart the destination server automatically if required unchecked (`Next >`)
* [ ] Press `Install` to process role installation
* [ ] Reboot if needed before configuration steps

### Configure Active Directory Domain Services

* [ ] After reboot, go back to Server Manager > Notifications > Promote this server to a domain controller
* [ ] In the configuration wizard, select `Add a new forest` and specify your domain name (e.g. `mydomain.com`) then press `Next >`
* [ ] In second step, keep forest and domain functionnal level to `Windows Server 2016` and select `Domain Name System (DNS) server` and `Global Catalog (GC)`
* [ ] Enter your DSRM password (`Next >`)
* [ ] In DNS options tab, keep `Create DNS delegation` unchecked (`Next >`)
* [ ] In Additional Options tab, the NetBIOS domain name should have been automatically completed (e.g. `MYDOMAIN`) then press `Next >`
* [ ] In Paths tab, keep default paths (`Next >`)
* [ ] Review selected Options and press `Next >`
* [ ] Prerequisites Check should detect some warnings, ignore them and click `Install`
* [ ] Reboot the server

### Install and configure Active Directory Certificate Services

* [ ] Go to server manager > Manage > Add Roles and Features
* [ ] Skip Before you begin page (`Next >`)
* [ ] Select Role-based or feature-based installation (`Next >`)
* [ ] Check `Select a server from the server pool` and check you domain controller is selected (`Next >`)
* [ ] Select `Active Directory Certificate Services` and click on `Add Features` in the popup (`Next >`)
* [ ] Check `Certification Authority` and `Certification Authority Web Enrollment` (second option in not mandatory)
* [ ] In Confirmation step, keep Restart the destination server automatically if required unchecked (`Next >`)
* [ ] Press `Install` to process role installation
* [ ] During installation, click on `Configure Active Directory Certificate Service on the destination server`
* [ ] In credentials wizard step, Credentials should be set to your current administrator account. Keep it and press `Next >`
* [ ] In Role Services step, check `Certification Authority` and `Certification Authority Web Enrollment` then `Next >`
* [ ] In Setup Type step, select `Enterprise CA` (`Next >`)
* [ ] In CA Type step, select `Root CA` (`Next >`)
* [ ] In Private Key step, select `Create a new private key` (`Next >`)
* [ ] In Criptography sub step, keep the default selection (`RSA#Microsoft Software Key Storage Provider`, `2048` for key length and `SHA256` algorithm) (`Next >`)
* [ ] In CA Name sub step, keep default values (`Next >`)
* [ ] In Validity Period, select the validity you want (10 years in my case) (`Next >`)
* [ ] In Certificate Database, keep default values (`Next >`)
* [ ] In Confirmation step, review the certificate service options and press `Configure`

### Export DC certificate

* [ ] Once the certificate services configuration is over, you can export your DC certificate public key
* [ ] Open a command prompt (`WIN+R`) and type `mmc`
* [ ] Go to File > Add/Remove snap-in
* [ ] Select `Certificates` and check `Computer Account` > `Next >` > `Local computer (...)` > `Finish` > `OK`
* [ ] Expand `Certificates (Local Computer)` > `Personal` > `Certificates`
* [ ] Right click on the certificate with the name format: `[domain]-[computer name]-CA` (in my case, `mydomain-DC01-CA`) > All Tasks > Export...
* [ ] On the wizard, check `No, I do not export the private key` then press `Next >`
* [ ] Select `DER encoded binary X.509 (.CER)` format then press `Next >`
* [ ] Specified the name of the exported file (in my case, I choose `dc01.mydomain.com.cer`) then press `Next >`
* [ ] Review the settings and press `Finish`
* [ ] Copy the .cer file to your local computer

## Keycloak configuration

To generate the truststore required by keycloak with msad, you must have jdk 1.8.x installed on your environment. Keytool is included with the jdk.  

### Generate your keystore with keytool

* [ ] Assuming your .cer file is under c:\ssl, open a command prompt, type `cmd`
* [ ] Type `c:\` then cd `c:\ssl` and enter the following commands
* [ ] `keytool -genkey -keyalg RSA -alias temp -keystore truststore.jks`
* [ ] `keytool -delete -alias temp -keystore truststore.ks`
* [ ] `keytool -import -v -trustcacerts -file dc01.mydomain.com.cer -keystore truststore.jks` (replace `dc01.mydomain.com.cer` with your file name)

>  Remember the password you specified during keystore generation, I will be used later.

### Run keycloak (clone this repository)

* [ ] Clone this repository (assuming you cloned it under d:\git\keycloak-with-msad)
* [ ] Replace empty ./assets/struststore.jks by yours (generated in previous step)
* [ ] Build this image running `docker build -t keycloak-msad .`
* [ ] Run `docker run -p 8080:8080 -e TRUSTSTORE_PWD=your-jks-password keycloak-msad`
* [ ] Connect to http://localhost:8080/auth to reach keycloak admin console with credentials `admin`, pwd: `admin`

### Keycloak provider configuration

* [ ] Once you're connected to keycloak admin console, go to `User Federation`
* [ ] Select `ldap` and press `add`
* [ ] In Settings tab, enter the following information
  * Import Users: `ON`
  * Edit Mode: `WRITABLE`
  * Sync Registrations: `ON`
  * Vendor: `Active Directory`
  * Connection URL: `ldaps://dc01.mydomain.com` (replace with your actual domain name)
  * Users DN: `CN=Users,DC=mydomain,DC=com`
  * Authentication Type: `simple`
  * Bind DN: `CN=Administrator,CN=Users,DC=mydomain,DC=com`
  * Bind Credential: `******` (administrator password)
  * Custom User LDAP Filter: `(&(objectClass=User)(objectCategory=Person))`
* [ ] Save this configuration and Synchronize all users